Looking back on my college years and experiences, 
I have learned a lot about web development and the endless possibilities that it creates. 
That is why I joined this bootcamp, 
to hone my skills and to better understand my capabilities in creating endless possibilites of web applications.

My previous work experience is a front-end developer intern. I usually managed the company's ui and ux website. 
I have been switched into a different role like back-end developer and a full-stack just to have a grasp on what
was happening behind the interface of the website. My experience gave me enough knowledge to understand the basics
and the flow of creating a fully functioning website.